import path from "path";

import fs from "fs-extra";
import ignore from "ignore";
import { watch, FSWatcher } from "chokidar";
import normalize from "normalize-path";
import { fromEvent, Observable } from "rxjs";
import { map, mergeAll } from "rxjs/operators";

import { File } from "./types";

// Wraps FSWatcher to manage events easier
export class Watcher {
  added: Observable<File>;
  changed: Observable<File>;
  removed: Observable<string>;
  errored: Observable<Error>;
  ready: Promise<boolean>;
  cwd: string;
  private watcher: FSWatcher;

  constructor(cwd: string) {
    this.cwd = cwd;
    const ig = ignore();
    // Ignore according to .gitignore (only at the project root).
    const gitignore = path.join(cwd, ".gitignore");
    if (fs.pathExistsSync(gitignore)) {
      ig.add(fs.readFileSync(gitignore, { encoding: "utf-8" }));
    }
    // Ignore according to .attachignore that uses .gitignore syntax.
    // Use this file to ignore files that's usually committed (awkward in .gitignore), but needs to be ignored when taking a challenge.
    // TODO this file should be always set to the original content when starting.
    const attachignore = path.join(cwd, ".attachignore");
    if (fs.pathExistsSync(attachignore)) {
      ig.add(fs.readFileSync(attachignore, { encoding: "utf-8" }));
    }
    // Always ignore node_modules and Attach related files.
    // Add these last to ensure nothing overwrites these rules.
    ig.add(["node_modules", ".qualified-attach.json", ".attachignore"]);

    // Set up watcher with `cwd` set to the workspace and watching any files under it.
    // This will ensure consistent relative path from the workspace to be emitted.
    const watcher = watch(".", {
      cwd,
      persistent: true,
      // If `false`, existing files are reported before "ready" event
      ignoreInitial: true,
      ignored: (absPath: string): boolean => {
        const relPath = path.relative(cwd, absPath);
        // Can be empty if absPath is cwd, `ig.ignores("")` throws.
        if (!relPath) return false;
        return ig.ignores(relPath);
      },
      // performance
      usePolling: false,
      interval: 100,
      binaryInterval: 300,
      // awaitWriteFinish: false, // default
      // Use this option to prevent removing a file triggering change event
      awaitWriteFinish: {
        // amount of time in milliseconds for a file size to remain constant before emitting this event
        stabilityThreshold: 200,
        // file size polling interval
        pollInterval: 100,
      },
      // errors
      ignorePermissionErrors: true,
      // atomic: true,
    });
    this.ready = new Promise(resolve => {
      watcher.on("ready", () => {
        resolve(true);
      });
    });

    this.watcher = watcher;

    // Set up Observers to be subscribed. We're not interested in directory events.
    // TODO Add filtering by filename and file contents to minimize noise.
    // Keeping these repetitive pipes until filtering is added.
    this.added = fromEvent<[string, fs.Stats?]>(watcher, "add").pipe(
      map(getFilePath),
      map(getContents(cwd)),
      mergeAll()
    );
    this.changed = fromEvent<[string, fs.Stats?]>(watcher, "change").pipe(
      map(getFilePath),
      map(getContents(cwd)),
      mergeAll()
    );
    this.removed = fromEvent<string>(watcher, "unlink");
    this.errored = fromEvent<Error>(watcher, "error");
  }

  close(): void {
    this.watcher.close();
  }

  async getWatchedFiles(): Promise<File[]> {
    const paths = await this.getWatchedFilePaths();
    return Promise.all(paths.map(getContents(this.cwd)));
  }

  private async getWatchedFilePaths(): Promise<string[]> {
    const watched = this.watcher.getWatched();
    const fpaths: string[] = [];
    for (const [dir, children] of Object.entries(watched)) {
      if (dir !== "..") {
        for (const c of children) fpaths.push(path.join(dir, c));
      }
    }
    return asyncFilter(fpaths, f => isFile(path.join(this.cwd, f)));
  }
}

async function asyncFilter<T>(
  xs: T[],
  predicate: (x: T, i: number, xs: T[]) => Promise<boolean>
): Promise<T[]> {
  const ps = await Promise.all(xs.map(predicate));
  return xs.filter((_, i) => ps[i]);
}

const isFile = async (file: string) => (await fs.lstat(file)).isFile();

const getFilePath = ([file, _]: [string, fs.Stats?]) => file;

const getContents = (cwd: string) => async (file: string) => ({
  path: normalize(file),
  contents: await fs.readFile(path.join(cwd, file), "utf-8"),
});
